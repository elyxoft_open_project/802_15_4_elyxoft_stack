# Elyxoft Stack

![General_MAC_frame_format](elyxoft_logo.png "https://www.elyxoft.fr/")  

Protocole développé par Elyxoft depuis 2020 sur le protocole 802.15.4.
La première version utilisée en 2020 devait permettre une communication rapide en portée directe.
Depuis ce protocole a évolué de façon à inclure la répétition de message et la gestion multi groupe non inclusive.

# 802.15.4

La stack Elyxoft est basée sur la spécification 802.15.4 et peut donc être implémentée au-dessus de tout composant supportant la couche mac 802.15.4 quelque soit la fréquence radio utilisée.
Du protocole 802.15.4 seul les messages de type « data » seront utilisés.

D’après la spécification « IEEE Std 802.15.4™-2003 », section 7.2 « MAC Frame formats » :


![General_MAC_frame_format](802_15_4_General_MAC_frame_format.png)  

![Format_of_the_frame_control_field](802_15_4_Format_of_the_frame_control_field.png)  

Elyxoft utilise uniquement les trames de type data et fixe le MHR de la trame avec les valeurs suivantes :  

## Frame control field (0x8841)

Frame type : Data (001)  
Security enable : False (0)  
Frame pending : False (0)  
Ack. request : False (0)  
Intra-PAN : True (1)  
Dest. Addressing mode : 16 bit short address (10)  
Source addressing mode : 16 bit short address (10)  

## Sequence number

Ce champ sera incrémenté à chaque envoi de trame. 

## Addressing fields

Destination PAN identifier : 'EL' pour identifier notre protocole (0x454C)
Destination address : Broadcast short address (0xFFFF)
Source Address : Adresse MAC de la carte (LSB-MSB)  

## MAC Payload

![MAC_PAYLOAD_DESCRIPTION](MAC_PAYLOAD_DESCRIPTION.png "Description du MAC PAYLOAD")  

1-2 : 16 bits réservés à l'identification du protocole  
3 : 4 bits de poid fort NB_Repeat => Nombre de fois qu'un message va se répeter (en décrémentant) par défaut : 0  
3 : 4 bits de poid faible Version => Version du Header  
4-5 : Adresse du groupe de la carte, 0xFFFF pour communiquer avec tout les groupes  
6 : Commande à effectuer  

## Filtrage du groupe

Un groupe X avec comme ID 00000000 00000011 peut communiquer avec les groupes 00000000 00000001 et 00000000 00000010
Ce qui se traduit par 0 != (my_group |incoming_group)  

# Reception

Voici un organigramme qui montre les étapes lors de la réception d'un packet :
![Organigramme_Reception](filtrage_organigramme.png)  

# Envoi

Pour envoyer une data il faut l'encoder dans un buffer d'envoi.
Le buffer a une taille maximale de 127 où chaque case représente 1 octet :

- Ajout du frame control 0X4188 ==> Buffer = |41|88|
- Ajout du sequence number ==> Buffer = |41|88|F8|
- Ajout du PAN ID 'EL' ==> Buffer = |41|88|F8|45|4C|
- Ajout de l'adresse de broadcast 0XFFFF ==> Buffer = |41|88|F8|45|4C|FF|FF|
- Ajout de son adresse ==> Buffer = |41|88|F8|45|4C|FF|FF|my_addr_msb|my_addr_lsb|
- Ajout du protocole ID 'EL' ==> Buffer = |41|88|F8|45|4C|FF|FF|my_addr_msb|my_addr_lsb|45|4C|
- Ajout de la répétion et de la version du header ==> Buffer = |41|88|F8|45|4C|FF|FF|my_addr_msb|my_addr_lsb|45|4C|01|
- Ajout de l'identifiant de son groupe ==> Buffer = |41|88|F8|45|4C|FF|FF|my_addr_msb|my_addr_lsb|45|4C|01|my_grp_msb|my_grp_lsb|
- Ajout de la commande ==> Buffer = |41|88|F8|45|4C|FF|FF|my_addr_msb|my_addr_lsb|45|4C|01|my_grp_msb|my_grp_lsb|..|
- Ajout de la data que l'on souhaite envoyer ==> Buffer = |41|88|F8|45|4C|FF|FF|my_addr_msb|my_addr_lsb|45|4C|01|my_grp_msb|my_grp_lsb|..|DATA|  

Buffer final = |41|88|F8|45|4C|FF|FF|my_addr_msb|my_addr_lsb|45|4C|01|my_grp_msb|my_grp_lsb|..|DATA...|RESERVED|RESERVED|
