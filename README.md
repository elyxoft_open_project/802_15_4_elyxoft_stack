# 802_15_4_ELYXOFT_STACK

Ce document est une description de la stack développée par Elyxoft au dessus de la couche MAC 802.15.4.  
Les références pour la couche MAC du protocole 802.15.4 sont celles de la spécification  « IEEE Std 802.15.4™-2003 », section 7.2 « MAC Frame formats ».  

La stack a pour but principal une communication rapide a portée directe mais peut évoluer afin de pouvoir ajouter de nouvelles fonctionnalitées au cours du temps.  
